#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import time
import pygame
import json  # import pickle  #TODO: Stop using pickle (because of security leaks)
try:
    import Tkinter as tkinter # this is for python2
    import tkFileDialog
except:
    import tkinter # this is for python3
    import tkinter.filedialog as tkFileDialog

from layer import Layer, _LayerPicklable
from settings import IMAGE_TILES, WIDTH, HEIGHT

class Game(object):

    window = None
    tile_images = {}
    layers = []
    active_layer_index = None
    font = None
    brushes = [
        [
            'grass',
            'water_left'
        ],
        [
            'tree',
            'water_full'
        ],
        [
            'hard_stone',
            'air'
        ],
        [
            'dark_stone',
            'air'
        ],
    ]
    brush = 0

    def __init__(self, *args, **kwargs):
        super(Game, self).__init__(*args, **kwargs)

        # init pygame, window
        pygame.init()
        self.window = pygame.display.set_mode((WIDTH, HEIGHT), pygame.DOUBLEBUF)
        self.font = pygame.font.SysFont("Arial", 20)

        # load images
        for tile_type, file_name in IMAGE_TILES.items():
            self.tile_images[tile_type] = self.load_image(file_name)

        # init layers
        self.layers = [
            Layer(self.window, self.tile_images),
            Layer(self.window, self.tile_images),
            Layer(self.window, self.tile_images),
            Layer(self.window, self.tile_images),
            Layer(self.window, self.tile_images),
            Layer(self.window, self.tile_images),
        ]
        self.active_layer_index = 0
        self.camX = 0
        self.camY = 0
        self.cvX  = 0
        self.cvY  = 0

    def save(self):
        filename = self.save_prompt()
        if not filename == "":
            list_to_save = []
            for i in self.layers:
                list_to_save.append(list(i.get_picklable().tiles.items()))
            f = open("%s.map" % (filename), "w")
            f.write(json.dumps(list_to_save)) # pickle.dump(i.get_picklable(), f)
            f.close()

    def load(self):
        filename = self.load_prompt()
        f = open("%s" % (filename), "r")
        contents_raw_raw = json.loads(f.read())
        f.close()
        _c = 0
        for contents_raw in contents_raw_raw:
            contents = dict()
            for x in contents_raw:
                contents.update({tuple(x[0]): x[1]})
            self.layers[_c] = _LayerPicklable(contents).get_real(self.window, self.tile_images) # pickle.load(f).get_real(self.window, self.tile_images) #WARNING: That's a security leak!
            _c += 1
    
    def save_prompt(self, title=None):
        root = tkinter.Tk()
        filename = tkFileDialog.asksaveasfilename(title=title, filetypes=((".map files","*.map"),))
        print(filename)
        root.destroy()
        return filename
    
    def load_prompt(self, title=None):
        root = tkinter.Tk()
        filename = tkFileDialog.askopenfilename(title=title, filetypes=((".map files","*.map"),))
        print(filename)
        root.destroy()
        return filename

    def load_image(self, filename):
        tmp = pygame.image.load(filename).convert_alpha()
        return pygame.transform.scale(
            tmp, 
            (32, 32)
        )

    def blit_text(self, text, color, coordinates, background=None, font=None):
        if not font:
            font = self.font

        self.window.blit(
            font.render(text, True, color, background),
            coordinates
        )

    def loop(self):
        legend_square = pygame.Surface((256, 128))
        show_legend = False
        pressed = 0
        
        self.brush = 0
        fElapsedTime = 0

        while 1:
            timerstart = time.perf_counter()
            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        pygame.quit()
                        sys.exit(0)
                    if event.key == pygame.K_y: #save layers to folder
                        self.save()
                    elif event.key == pygame.K_x: #load layers from folder
                        self.load()
                    elif event.key == pygame.K_c:
                        pass
                    elif event.key == pygame.K_g:
                        show_legend = ~(show_legend)
                    elif event.key == pygame.K_1:
                        self.brush -= 1
                        if self.brush < 0:
                            self.brush += 1
                    elif event.key == pygame.K_2:
                        self.brush += 1
                        if self.brush >= len(self.brushes):
                            self.brush -= 1
                    elif event.key == pygame.K_w:
                        self.cvY = -10 * fElapsedTime
                    elif event.key == pygame.K_s:
                        self.cvY = 10 * fElapsedTime
                    elif event.key == pygame.K_a:
                        self.cvX = -10 * fElapsedTime
                    elif event.key == pygame.K_d:
                        self.cvX = 10 * fElapsedTime
                
                elif event.type == pygame.KEYUP:
                    if event.key == pygame.K_w:
                        self.cvY = 0
                    elif event.key == pygame.K_s:
                        self.cvY = 0
                    elif event.key == pygame.K_a:
                        self.cvX = 0
                    elif event.key == pygame.K_d:
                        self.cvX = 0

                elif event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit(0)

                elif event.type == pygame.MOUSEBUTTONDOWN:
                    if event.button == 1:
                        # mouse left button
                        pressed = 1

                    elif event.button == 3:
                        # mouse right button
                        pressed = 3

                    elif event.button == 4:
                        # mouse wheel up
                        self.active_layer_index += 1
                        if self.active_layer_index > len(self.layers) - 1:
                            self.active_layer_index = 0

                    elif event.button == 5:
                        # mouse wheel down
                        self.active_layer_index -= 1
                        if self.active_layer_index < 0:
                            self.active_layer_index = len(self.layers) - 1

                elif event.type == pygame.MOUSEBUTTONUP:
                    if event.button == 1:
                        pressed = 0
                    elif event.button == 3:
                        pressed = 0
            
            pen =  self.brushes[self.brush][0]
            pen2 = self.brushes[self.brush][1]

            self.camX += self.cvX
            self.camY += self.cvY
            
            self.window.fill((0, 0, 0))
            legend_square.fill((128, 0, 0))

            if pressed == 1:
                pos = pygame.mouse.get_pos()
                layer_pos = (int(pos[0] / 32), int(pos[1] / 32))
                self.layers[self.active_layer_index].add_tile(layer_pos[0] + int(self.camX), layer_pos[1] + int(self.camY), pen)
            elif pressed == 3:
                pos = pygame.mouse.get_pos()
                layer_pos = (int(pos[0] / 32), int(pos[1] / 32))
                self.layers[self.active_layer_index].add_tile(layer_pos[0] + int(self.camX), layer_pos[1] + int(self.camY), pen2)

            # render layers
            for layer in self.layers:
                layer.render(int(self.camX), int(self.camY))

            if show_legend:
                self.window.blit(legend_square, (WIDTH - legend_square.get_width(), 0))

                tmp = "Layer: %d" % (self.active_layer_index)
                self.blit_text(tmp, (1,1,1), (WIDTH - self.font.size(tmp)[0] - 10, 0))
                tmp2 = self.font.size(tmp)

                tmp = "Tiles: "
                self.blit_text(tmp, (1,1,1), (WIDTH - self.font.size(tmp)[0] - 70, tmp2[1] + 3))
                self.window.blit(self.tile_images[pen2], (WIDTH - 35, tmp2[1] + 3))
                self.window.blit(self.tile_images[pen], (WIDTH - 69, tmp2[1] + 3))
                tmp2 = self.font.size(tmp)

            pygame.display.flip()
            fElapsedTime = time.perf_counter() - timerstart


if __name__ == "__main__":
    game = Game()
    game.loop()
