
IMAGE_TILES = {
    'grass': './images/grass.png',
    'tree': './images/tree.png',
    'air': './images/air.png',
    'dark_stone': './images/dark_stone.png',
    'hard_stone': './images/hard_stone.png',
    'water_full': './images/water/water_full.png',
    'water_downleft': './images/water/water_downleft.png',
    'water_upleft': './images/water/water_upleft.png',
    'water_downright': './images/water/water_downright.png',
    'water_upright': './images/water/water_downright.png',
    'water_right': './images/water/water_right.png',
    'water_left': './images/water/water_left.png',
    'water_up': './images/water/water_up.png',
    'water_down': './images/water/water_down.png',
}

WIDTH = 600
HEIGHT = 600
