class Layer(object):

    window = None

    def __init__(self, window, tile_images, *args, **kwargs):
        super(Layer, self).__init__(*args, **kwargs)
        
        self.tiles = {}
        self.window = window
        self.tile_images = tile_images

    def get_picklable(self):
        """This function was created to avoid limitations of Surface objects"""
        return _LayerPicklable(self.tiles)

    def clear(self):
        self.tiles = {}
        self.window.fill((0, 0, 0))

    def add_tile(self, x, y, _type):
        if _type == "air":
            if (x, y) in self.tiles: #Optimisation
                del self.tiles[(x, y)]
            return
        self.tiles[(x, y)] = _type

    def render(self, offsetX, offsetY):
        for position, _type in self.tiles.items():
            pos = (position[0]*32 - offsetX*32, position[1]*32 - offsetY*32)
            self.window.blit(self.tile_images[_type], pos)



class _LayerPicklable(object):
    
    """Layer object generator."""
    
    def __init__(self, tiles, *args, **kwargs):
        super(_LayerPicklable, self).__init__(*args, **kwargs)
        self.tiles = tiles

    def get_real(self, window, tile_images):
        real = Layer(window, tile_images)
        real.tiles = self.tiles
        return real
